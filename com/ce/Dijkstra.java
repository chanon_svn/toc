package com.ce;

import java.util.PriorityQueue;
import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;
//import java.io.BufferedReader;
import java.io.*;

public class Dijkstra
{
    public static void computePaths(Vertex source)
    {
        source.minDistance = 0.;
        PriorityQueue<Vertex> vertexQueue = new PriorityQueue<Vertex>();
      	vertexQueue.add(source);

        while (!vertexQueue.isEmpty()) {
	       Vertex u = vertexQueue.poll();

            // Visit each edge exiting u
            for (Edge e : u.adjacencies)
            {
                System.out.println("target:");
                System.out.println(e.target + " " + e.weight);
                Vertex v = e.target;
                double weight = e.weight;
                double distanceThroughU = u.minDistance + weight;
        		if (distanceThroughU < v.minDistance) {
        		    vertexQueue.remove(v);
        		    v.minDistance = distanceThroughU;
        		    v.previous = u;
        		    vertexQueue.add(v);
        		}
            }
        }
    }

    public static List<Vertex> getShortestPathTo(Vertex target)
    {
        List<Vertex> path = new ArrayList<Vertex>();
        System.out.println("in getShortestPathTo");
        for (Vertex vertex = target; vertex != null; vertex = vertex.previous){
            //System.out.println(vertex.toString());
            path.add(vertex);
        }
        Collections.reverse(path);
        return path;
    }

    public static void main(String[] args)
    {
/*        Vertex a = new Vertex("a");
    	Vertex b = new Vertex("b");
    	Vertex c = new Vertex("c");
    	Vertex d = new Vertex("d");
    	Vertex e = new Vertex("e");
        Vertex f = new Vertex("f");
        Vertex g = new Vertex("g");
        Vertex h = new Vertex("h");
        Vertex i = new Vertex("i");
        Vertex j = new Vertex("j");
        Vertex k = new Vertex("k");
        Vertex z = new Vertex("z");

    	a.adjacencies = new ArrayList<Edge>();
        Collections.addAll(a.adjacencies, new Edge(c, 8),
    	                            new Edge(b, 3),
                                    new Edge(g, 7),
                                    new Edge(k, 2));
    	b.adjacencies = new ArrayList<Edge>();
         Collections.addAll(b.adjacencies, new Edge(a, 3),
    	                            new Edge(e, 4),
                                    new Edge(d, 1),
    	                            new Edge(f, 5));
    	c.adjacencies = new ArrayList<Edge>();
         Collections.addAll(c.adjacencies, new Edge(a, 8),
                                    new Edge(e, 3));
    	d.adjacencies = new ArrayList<Edge>();
         Collections.addAll(d.adjacencies, new Edge(b, 1),
                                    new Edge(g, 2),
                                    new Edge(f, 7),
    	                            new Edge(d, 1));
    	e.adjacencies = new ArrayList<Edge>();
         Collections.addAll(e.adjacencies, new Edge(c, 3),
                                    new Edge(b, 4),
                                    new Edge(f, 1)) ;
        f.adjacencies = new ArrayList<Edge>();
         Collections.addAll(f.adjacencies, new Edge(e, 1),
                                    new Edge(b, 5),
                                    new Edge(d, 7),
                                    new Edge(z, 2));
        g.adjacencies = new ArrayList<Edge>();
         Collections.addAll(g.adjacencies, new Edge(a, 7),
                                    new Edge(k, 3),
                                    new Edge(d, 3)) ;
        h.adjacencies = new ArrayList<Edge>();
         Collections.addAll(h.adjacencies, new Edge(k, 4),
                                    new Edge(i, 7));
        i.adjacencies = new ArrayList<Edge>();
         Collections.addAll(i.adjacencies, new Edge(j, 4),
                                    new Edge(h, 7),
                                    new Edge(z, 3));
        j.adjacencies = new ArrayList<Edge>();
         Collections.addAll(j.adjacencies, new Edge(d, 2),
                                    new Edge(i, 4));
        k.adjacencies = new ArrayList<Edge>();
         Collections.addAll(k.adjacencies, new Edge(a, 2),
                                    new Edge(g, 3),
                                    new Edge(h, 4));
        z.adjacencies = new ArrayList<Edge>();
         Collections.addAll(z.adjacencies, new Edge(f, 2),
                                    new Edge(i, 3));
    	Vertex[] vertices = { a, b, c, d, e, f, i, j, k, z }; */
        
        ArrayList<Vertex> vertices = new ArrayList<Vertex>();
        int round = 0;
      /*  Scanner scanner = new Scanner(System.in);
        System.out.println("Enter path file : ");
        String path = scanner.nextLine();*/
        try{
            FileReader file = new FileReader("/home/dscanon/Documents/toc/toc/file.csv");
            BufferedReader reader = new BufferedReader(file); 
            String line = null;
        
            while ((line =reader.readLine())!=null) {
               String[] splitlist = line.split(",");
                for(int count=0;count<splitlist.length;count++)
                {
                    if(round == 0)
                    {
                        Vertex tempvertex = new Vertex(splitlist[count]);
                        tempvertex.adjacencies = new ArrayList<Edge>(); 
                        vertices.add(tempvertex);
                        System.out.println(vertices.get(count).toString());
                    }
                    else{
                        if(!splitlist[count].equals("0"))
                        {
                            vertices.get(round-1).adjacencies.add(new Edge(vertices.get(count),  Double.parseDouble(splitlist[count])));
                        }  
                    }

                }
                round++;
            }
        }
        catch(IOException ioe){
            System.out.println(ioe);
        }


    /*
        Scanner scanner = new Scanner(System.in);
        System.out.println("Insert Start Vertice : ");
        String starter = scanner.nextLine();
        System.out.println("Insert End Vertice : ");
        String ender = scanner.nextLine();
        
        Vertex start = new Vertex(starter);
        Vertex end = new Vertex(ender);

        switch(starter)
        {
            case "a" :   start = a; break;
            case "b" :   start = b; break;
            case "c" :   start = c; break;
            case "d" :   start = d; break;
            case "e" :   start = e; break;
            case "f" :   start = f; break;
            case "g" :   start = g; break;
            case "h" :   start = h; break;
            case "i" :   start = i; break;
            case "j" :   start = j; break;
            case "k" :   start = k; break;
            case "z" :   start = z; break;
            default:  break;
        }
         switch(ender)
        {
            case "a" :   end = a; break;
            case "b" :   end = b; break;
            case "c" :   end = c; break;
            case "d" :   end = d; break;
            case "e" :   end = e; break;
            case "f" :   end = f; break;
            case "g" :   end = g; break;
            case "h" :   end = h; break;
            case "i" :   end = i; break;
            case "j" :   end = j; break;
            case "k" :   end = k; break;
            case "z" :   end = z; break;
            default:  break;
        }
        */

        for(int ii=0;ii<vertices.size();ii++){
            vertices.get(ii).printAdjacen();
        }
        //System.out.println(vertices.get(0).toString());
        //System.out.println(vertices.get(11).toString());
        computePaths(vertices.get(0));

    	List<Vertex> path = getShortestPathTo(vertices.get(11));
    	System.out.println("Path: " + path);
        
    }
}
