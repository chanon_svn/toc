package com.ce;

import java.util.ArrayList;

class Vertex implements Comparable<Vertex>
{
    public final String name;
    public ArrayList<Edge> adjacencies;
    public double minDistance = Double.POSITIVE_INFINITY;
    public Vertex previous;
    public Vertex(String argName) { name = argName; }
    public String toString() { return name; }
    public int compareTo(Vertex other)
    {
        return Double.compare(minDistance, other.minDistance);
    }

    public void printAdjacen(){
        System.out.println(this.name);
        for(int i=0; i<this.adjacencies.size() ; i++){
            System.out.println(this.adjacencies.get(i).target.toString() + " " + this.adjacencies.get(i).weight );
        }
    }
}