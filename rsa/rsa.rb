class Integer
  def prime?
    n = self.abs
     return true if n == 2
     return false if n == 1 || n & 1 == 0
     return false if n > 3 && n % 6 != 1 && n % 6 != 5

     d = n-1
     d >>= 1 while d & 1 == 0
     20.times do
       a = rand(n-2) + 1
       t = d
       y = Integer.mod_pow( a, t, n )
       while t != n-1 && y != 1 && y != n-1
         y = (y * y) % n
         t <<= 1
       end
       return false if y != n-1 && t & 1 == 0
     end
     return true 
  end

  def self.mod_pow( base, power, mod )
    res = 1
    while power > 0
      res = (res * base) % mod if power & 1 == 1
      base = base ** 2 % mod
      power >>= 1
    end
    res
  end 
end

class RSA
  E = 65537

  class << self
    def generate_key
        n,d = 0
        p = random_prime
        q = random_prime 
        n = p * q
        d = get_d(p, q, E)
        [n, E, d]
    end

    def encrypt(m,n)
      Integer.mod_pow(m, E, n)
    end

    def decrypt(c, n, d)
      Integer.mod_pow(c, d, n)
    end

    def random_prime
      begin
        n = random_number
        return n if n.prime?
      end while true
    end

    def random_number
      rand(2**30..2**32)
    end

    def phi(a,b)
      (a-1) * (b-1)
    end

    def extended_gcd(a,b)
      return [0,1] if a%b == 0
      x,y = extended_gcd(b, a%b)
      [y, x-y*(a/b)]
    end

    def get_d(p, q, e)
      t = phi(p,q)
      x, y = extended_gcd(e,t)
      x += t if x < 0
      x
    end
  end
end


m = (496 * 280)

n,e,d = RSA.generate_key

puts "public exponent: %d" % e
puts "public modulus : %d" % n
puts "private exp    : %d" % d

puts ""
puts "message         : %d" % m

puts ""

c = RSA.encrypt(m, n)

puts "Encrypted       : %d" % c
puts ""
p = RSA.decrypt(c, n, d)
puts "Decrypted    : %s" % p
puts (m == p)?"Succeed":"Fail"